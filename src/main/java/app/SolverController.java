package app;

import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2018/7/21 0021
 * Time: 10:25
 * To change this template use File | Settings | File Templates.
 * Description:
 */
public class SolverController implements Initializable {
    private final SolverService solverService;
    public Button openButton;
    public GridPane grid;
    public ToolBar toolBar;
    private FileChooser fileChooser;

    public SolverController() {
        solverService = new SolverService();

    }

    /**
     * 初始化打开对话框
     */
    private void initFileChooser() {
        fileChooser = new FileChooser();
        fileChooser.setTitle("\u52a0\u8f7d\u9898\u76ee");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("\u9898\u76ee\u6587\u4ef6", "*.txt"));
    }

    /**
     * 加载题目
     *
     * @param actionEvent
     */
    public void open(MouseEvent actionEvent) {
        File file = fileChooser.showOpenDialog(null);
        if (file == null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("\u6d88\u606f");
            alert.setHeaderText("\u672a\u9009\u62e9\u9898\u76ee\u6587\u4ef6!");
            alert.showAndWait();
            return;
        }
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            int[][] question = new int[9][9];
            String line = "";
            for (int i = 0; i < 9; i++) {
                line = bufferedReader.readLine();
                for (int j = 0; j < 9; j++) {
                    question[i][j] = Integer.parseInt(String.valueOf(line.charAt(j)));
                }
            }
            solverService.setQuestion(question);
            solve();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解题
     */
    private void solve() {
        solverService.fill(0);
        drawSolution();
    }

    /**
     * 打印答案
     */
    private void drawSolution() {
        grid.getChildren().clear();
        grid.setGridLinesVisible(true);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Label label = createLabel(i, j);
                grid.add(label, j, i);//注意行列号
            }
        }
    }

    private Label createLabel(int i, int j) {
        Label label = new Label(String.valueOf(solverService.getSolution()[i][j]));
        label.setFont(Font.font(36));
        if (solverService.getSolution()[i][j] == solverService.getQuestion()[i][j]) {
            label.setTextFill(Color.DARKSLATEGRAY);
        } else {
            label.setTextFill(Color.LAWNGREEN);
        }
        GridPane.setValignment(label, VPos.CENTER);
        GridPane.setHalignment(label, HPos.CENTER);
        return label;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initFileChooser();
    }
}
